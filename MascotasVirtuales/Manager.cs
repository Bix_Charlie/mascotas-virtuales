﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MascotasVirtuales
{
    class Manager : Mascota
    {
        public Mascota[] listaMascotas = new Mascota[3] { new Mascota(), new Mascota(), new Mascota() } ;
        public Manager()
        {

        }

        public Manager(int numeroMascotasRandom)
        {
            for (int i = 0; i < numeroMascotasRandom; i++)
            {
                listaMascotas[i] = crearMascotaRandom();
            }
        }

        public int numeroMascotas()
        {
            int numMascota = 0;
            Console.WriteLine("¿Cuantas mascotas vas a crear?");
            numMascota = Convert.ToInt32(Console.ReadLine());
            return numMascota;
        }

        public enum especies
        {
            perro = 1,
            gato,
            raton,
            conejo,
            dragon
        }

        public static string checkarEspecie(int especie)
        {
            string resultado = "";
            switch (especie)
            {
                case (int)especies.perro:
                    resultado += "perro";
                    break;
                case (int)especies.gato:
                    resultado += "gato";
                    break;
                case (int)especies.raton:
                    resultado += "raton";
                    break;
                case (int)especies.conejo:
                    resultado += "conejo";
                    break;
                case (int)especies.dragon:
                    resultado += "dragon";
                    break;
                default:
                    resultado += "NULL";
                    break;
            }

            return resultado;
        }
        public void GuardarMascota(Mascota newmascota)
        {
            listaMascotas[1] = newmascota;
        }
        private static Random random = new Random();
        public Mascota crearMascotaRandom()
        {
            /*int opcion;
            Console.WriteLine("Que mascota quieres? \n1-Perro \n2-Gato \n3-Raton \n4-Conejo \n5-Dragon \n6-Regresar");
            opcion = Convert.ToInt32(Console.ReadLine());
            return opcion;
            */
            return seleccionMascota(random.Next(1, Enum.GetNames(typeof(especies)).Length));
        }

        public Mascota seleccionMascota(int op)
        {
            Mascota masc = new Mascota();
            switch (op)
            {
                case 1:
                    masc = new Mascota("blanco", "Toreto", (int)especies.perro);
                    break;
                case 2:
                    masc= new Mascota("gris", "Travel", (int)especies.gato);
                    break;
                case 3:
                    masc= new Mascota("azul", "Mr meesseks", (int)especies.raton);
                    break;
                case 4:
                    masc = new Mascota("negro", "Satan", (int)especies.conejo);
                    break;
                case 5:
                    masc= new Mascota("Rojo", "Flame", (int)especies.dragon);
                    break;

                default:
                        Console.WriteLine("Regresando...");
                        Console.ReadKey();
                        break;
            }
            return masc;
        }

        public void mostrarMascotas(Mascota[] lista)
        {
            for (int i = 0; i < lista.Length; i++)
            {
                Console.WriteLine(lista[i]);
            }
        }

        private void swap(int a, int b)
        {
            Mascota temp;
            temp = listaMascotas[a];
            listaMascotas[a] = listaMascotas[b];
            listaMascotas[b] = temp;
        }

        public void acomodarMascotasPorFelicidad()
        {
            int i, j;
            int N = listaMascotas.Length;

            for (j = N - 1; j > 0; j--)
            {
                for (i = 0; i < j; i++)
                {
                    if (listaMascotas[i].Felicidad < listaMascotas[i + 1].Felicidad) swap(i, i + 1);
                }
            }
        }

        /*public void mostrarFelicidad(Mascota[] feli)
        {

            for (int i = 0; i < feli.Length - 1; i++)
            {
                for (int j = 0; j < feli.Length - i - 1; j++)
                {
                    if (feli[j].Felicidad > feli[j + 1].Felicidad)
                    {
                        Mascota temp = feli[j];
                        feli[j] = feli[j + 1];
                        feli[j + 1] = temp;
                    }
                }

            }
            for (int i = 0; i < feli.Length; i++)
            {
                Console.WriteLine(feli[i].Felicidad);
            }
        }
        */

        public override string ToString()
        {
            return "Nombre" + Nombre + "Color" + Color + "Especie" + Especie + "Vivo";
        }
    }
}
