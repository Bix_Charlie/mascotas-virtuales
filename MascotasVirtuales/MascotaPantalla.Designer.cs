﻿namespace MascotasVirtuales
{
    partial class MascotaPantalla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MascotaPantalla));
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtMascota = new System.Windows.Forms.Label();
            this.pbMascota = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btDetalles = new System.Windows.Forms.Button();
            this.btBaniar = new System.Windows.Forms.Button();
            this.btJugar = new System.Windows.Forms.Button();
            this.btAlimentar = new System.Windows.Forms.Button();
            this.txtDetalles = new System.Windows.Forms.Label();
            this.panelDetalles = new System.Windows.Forms.Panel();
            this.btOk = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMascota)).BeginInit();
            this.panel3.SuspendLayout();
            this.panelDetalles.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.txtMascota);
            this.panel2.Location = new System.Drawing.Point(15, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(325, 59);
            this.panel2.TabIndex = 1;
            // 
            // txtMascota
            // 
            this.txtMascota.AutoSize = true;
            this.txtMascota.Font = new System.Drawing.Font("Sniglet", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMascota.Location = new System.Drawing.Point(18, 17);
            this.txtMascota.Name = "txtMascota";
            this.txtMascota.Size = new System.Drawing.Size(83, 24);
            this.txtMascota.TabIndex = 0;
            this.txtMascota.Text = "Mascota";
            // 
            // pbMascota
            // 
            this.pbMascota.BackColor = System.Drawing.Color.Transparent;
            this.pbMascota.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbMascota.Image = ((System.Drawing.Image)(resources.GetObject("pbMascota.Image")));
            this.pbMascota.Location = new System.Drawing.Point(37, 77);
            this.pbMascota.Name = "pbMascota";
            this.pbMascota.Size = new System.Drawing.Size(289, 241);
            this.pbMascota.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMascota.TabIndex = 2;
            this.pbMascota.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.btDetalles);
            this.panel3.Controls.Add(this.btBaniar);
            this.panel3.Controls.Add(this.btJugar);
            this.panel3.Controls.Add(this.btAlimentar);
            this.panel3.Location = new System.Drawing.Point(12, 324);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(328, 74);
            this.panel3.TabIndex = 6;
            // 
            // btDetalles
            // 
            this.btDetalles.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btDetalles.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btDetalles.BackgroundImage")));
            this.btDetalles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btDetalles.Location = new System.Drawing.Point(249, 3);
            this.btDetalles.Name = "btDetalles";
            this.btDetalles.Size = new System.Drawing.Size(76, 68);
            this.btDetalles.TabIndex = 3;
            this.btDetalles.UseVisualStyleBackColor = false;
            this.btDetalles.Click += new System.EventHandler(this.btDetalles_Click);
            // 
            // btBaniar
            // 
            this.btBaniar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btBaniar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btBaniar.BackgroundImage")));
            this.btBaniar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btBaniar.Location = new System.Drawing.Point(167, 3);
            this.btBaniar.Name = "btBaniar";
            this.btBaniar.Size = new System.Drawing.Size(76, 68);
            this.btBaniar.TabIndex = 2;
            this.btBaniar.UseVisualStyleBackColor = false;
            this.btBaniar.Click += new System.EventHandler(this.btBaniar_Click);
            // 
            // btJugar
            // 
            this.btJugar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btJugar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btJugar.BackgroundImage")));
            this.btJugar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btJugar.Location = new System.Drawing.Point(85, 3);
            this.btJugar.Name = "btJugar";
            this.btJugar.Size = new System.Drawing.Size(76, 68);
            this.btJugar.TabIndex = 1;
            this.btJugar.UseVisualStyleBackColor = false;
            this.btJugar.Click += new System.EventHandler(this.btJugar_Click);
            // 
            // btAlimentar
            // 
            this.btAlimentar.BackColor = System.Drawing.Color.White;
            this.btAlimentar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btAlimentar.BackgroundImage")));
            this.btAlimentar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btAlimentar.Location = new System.Drawing.Point(3, 3);
            this.btAlimentar.Name = "btAlimentar";
            this.btAlimentar.Size = new System.Drawing.Size(76, 68);
            this.btAlimentar.TabIndex = 0;
            this.btAlimentar.UseVisualStyleBackColor = false;
            this.btAlimentar.Click += new System.EventHandler(this.btAlimentar_Click);
            // 
            // txtDetalles
            // 
            this.txtDetalles.AutoSize = true;
            this.txtDetalles.Font = new System.Drawing.Font("Sniglet", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDetalles.Location = new System.Drawing.Point(12, 12);
            this.txtDetalles.Name = "txtDetalles";
            this.txtDetalles.Size = new System.Drawing.Size(67, 20);
            this.txtDetalles.TabIndex = 7;
            this.txtDetalles.Text = "Detalles";
            // 
            // panelDetalles
            // 
            this.panelDetalles.Controls.Add(this.btOk);
            this.panelDetalles.Controls.Add(this.txtDetalles);
            this.panelDetalles.Location = new System.Drawing.Point(37, 135);
            this.panelDetalles.Name = "panelDetalles";
            this.panelDetalles.Size = new System.Drawing.Size(289, 120);
            this.panelDetalles.TabIndex = 8;
            this.panelDetalles.Visible = false;
            // 
            // btOk
            // 
            this.btOk.Location = new System.Drawing.Point(106, 85);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(63, 32);
            this.btOk.TabIndex = 9;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // MascotaPantalla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.ClientSize = new System.Drawing.Size(360, 410);
            this.Controls.Add(this.panelDetalles);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pbMascota);
            this.Controls.Add(this.panel2);
            this.Name = "MascotaPantalla";
            this.Text = "MascotaPantalla";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMascota)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panelDetalles.ResumeLayout(false);
            this.panelDetalles.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pbMascota;
        private System.Windows.Forms.Label txtMascota;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btAlimentar;
        private System.Windows.Forms.Button btJugar;
        private System.Windows.Forms.Button btBaniar;
        private System.Windows.Forms.Button btDetalles;
        private System.Windows.Forms.Label txtDetalles;
        private System.Windows.Forms.Panel panelDetalles;
        private System.Windows.Forms.Button btOk;
    }
}