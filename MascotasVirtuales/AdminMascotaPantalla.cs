﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MascotasVirtuales
{
    public partial class AdminMascotaPantalla : Form
    {
        Mascota nuevaMascota = new Mascota();
        public AdminMascotaPantalla()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btPerro_Click(object sender, EventArgs e)
        {
            nuevaMascota.Especie = 1;
            btGato.Visible = false;
            btRaton.Visible = false;
            btConejo.Visible = false;
            btDragon.Visible = false;
            gbNombre.Visible = true;
            btPerro.Location = new Point(100, 100);
        }

        private void btGato_Click(object sender, EventArgs e)
        {
            nuevaMascota.Especie = 2;
            btPerro.Visible = false;
            btRaton.Visible = false;
            btConejo.Visible = false;
            btDragon.Visible = false;
            gbNombre.Visible = true;
            btGato.Location = new Point(100, 100);
        }

        private void btConejo_Click(object sender, EventArgs e)
        {
            nuevaMascota.Especie = 3;
            btPerro.Visible = false;
            btRaton.Visible = false;
            btGato.Visible = false;
            btDragon.Visible = false;
            gbNombre.Visible = true;
            btConejo.Location = new Point(100, 100);
        }

        private void btRaton_Click(object sender, EventArgs e)
        {
            nuevaMascota.Especie = 4;
            btPerro.Visible = false;
            btGato.Visible = false;
            btConejo.Visible = false;
            btDragon.Visible = false;
            gbNombre.Visible = true;
            btRaton.Location = new Point(100, 100);
        }

        private void btDragon_Click(object sender, EventArgs e)
        {
            nuevaMascota.Especie = 5;
            btPerro.Visible = false;
            btRaton.Visible = false;
            btGato.Visible = false;
            btConejo.Visible = false;
            gbNombre.Visible = true;
            btDragon.Location = new Point(100, 100);
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            nuevaMascota.Nombre = txtNombreAnimal.Text;
            MessageBox.Show("Se ha creado una mascota de especie:" + nuevaMascota.checkarEspecie(nuevaMascota.Especie) + " y de nombre: " + nuevaMascota.Nombre, "Nueva mascota", MessageBoxButtons.OK);
            Manager guardado = new Manager();
            guardado.GuardarMascota(nuevaMascota);
            Close();
        }
    }
}
