﻿namespace MascotasVirtuales
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayMascotas = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.btNuevas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // displayMascotas
            // 
            this.displayMascotas.Location = new System.Drawing.Point(38, 26);
            this.displayMascotas.Name = "displayMascotas";
            this.displayMascotas.Size = new System.Drawing.Size(712, 336);
            this.displayMascotas.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(38, 390);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(236, 38);
            this.button1.TabIndex = 1;
            this.button1.Text = "Organizar por Felicidad";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btNuevas
            // 
            this.btNuevas.Location = new System.Drawing.Point(514, 390);
            this.btNuevas.Name = "btNuevas";
            this.btNuevas.Size = new System.Drawing.Size(236, 38);
            this.btNuevas.TabIndex = 3;
            this.btNuevas.Text = "Crear nuevas mascotas";
            this.btNuevas.UseVisualStyleBackColor = true;
            this.btNuevas.Click += new System.EventHandler(this.btNuevas_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btNuevas);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.displayMascotas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel displayMascotas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btNuevas;
    }
}