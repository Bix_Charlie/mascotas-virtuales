﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MascotasVirtuales
{
    public partial class Form1 : Form
    {
        Manager master = new Manager(3);
        //Se crea un boton para cada elemento de la lista
        Random rand = new Random();
        public Form1()
        {
            InitializeComponent();
            foreach (Mascota mascota in master.listaMascotas)
            {
                Button nuevoBoton = new Button();
                mascota.Felicidad = rand.Next(1, 20);
                nuevoBoton.Text = mascota.Nombre +"  "+ mascota.Felicidad;
                nuevoBoton.Font = new Font(nuevoBoton.Font.FontFamily, 20f);
                nuevoBoton.AutoSize = true;
                nuevoBoton.Click += (sender, e) => OnBtnClick(sender,e,mascota); //se le asigna a cada boton la funcionalidad en click para abrir una pantalla
                displayMascotas.Controls.Add(nuevoBoton);//se agrega el boton creado al display de flowLayout
            }
        }
        void OnBtnClick(object sender, EventArgs e, Mascota mascota)
        {
            new MascotaPantalla(mascota).Show();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            displayMascotas.Controls.Clear();
            master.acomodarMascotasPorFelicidad();
            foreach (Mascota mascota in master.listaMascotas)
            {
                Button nuevoBoton = new Button();
                //mascota.Felicidad = rand.Next(1, 20);
                nuevoBoton.Text = mascota.Nombre + "  " + mascota.Felicidad;
                nuevoBoton.Font = new Font(nuevoBoton.Font.FontFamily, 20f);
                nuevoBoton.AutoSize = true;
                nuevoBoton.Click += (sender2, e2) => OnBtnClick(sender, e, mascota); //se le asigna a cada boton la funcionalidad en click para abrir una pantalla
                displayMascotas.Controls.Add(nuevoBoton);//se agrega el boton creado al display de flowLayout
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new AdminMascotaPantalla().Show();
        }

        private void btNuevas_Click(object sender, EventArgs e)
        {
            new PantallaInicio().Show();
        }
    }

}
