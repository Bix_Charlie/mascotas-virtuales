﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MascotasVirtuales
{
    public partial class MascotaPantalla : Form
    {
        Mascota pet = new Mascota();

        public MascotaPantalla()
        {
            InitializeComponent();
        }
        public MascotaPantalla(Mascota pet)
        {
            InitializeComponent();
            this.pet = pet;
            txtMascota.Text = pet.Nombre + " el " + pet.checkarEspecie(pet.Especie) + " " + " Color: " + pet.Color;
            
            if(pet.checkarEspecie(pet.Especie) == "perro" )
            {
                pbMascota.Image = MascotasVirtuales.Properties.Resources.perro;
            }   
            else if(pet.checkarEspecie(pet.Especie) == "gato")
            {
                pbMascota.Image = MascotasVirtuales.Properties.Resources.gato;
            }
            else if (pet.checkarEspecie(pet.Especie) == "raton")
            {
                pbMascota.Image = MascotasVirtuales.Properties.Resources.rata;            
            }
            else if (pet.checkarEspecie(pet.Especie) == "dragon")
            {
                pbMascota.Image = MascotasVirtuales.Properties.Resources.dragon;
            }
            else if (pet.checkarEspecie(pet.Especie) == "conejo")
            {
                pbMascota.Image = MascotasVirtuales.Properties.Resources.bunny;
            }

        }

        private void btAlimentar_Click(object sender, EventArgs e)
        {
            if (!pet.checkarMuerte())
            {
                pet.alimentar();
                MessageBox.Show("Has alimentado al " + Manager.checkarEspecie(pet.Especie), ":)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Tu mascota ha muerto :( ", ":(", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }     
        }

        private void btJugar_Click(object sender, EventArgs e)
        {
            if (!pet.checkarMuerte())
            {
                pet.jugar();
                MessageBox.Show("Has jugado con el " + Manager.checkarEspecie(pet.Especie), ":)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Tu mascota ha muerto :( ", ":(", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            } 
        }

        private void btBaniar_Click(object sender, EventArgs e)
        {
            if (!pet.checkarMuerte())
            {
                pet.bañar();
                MessageBox.Show("Has bañado al " + Manager.checkarEspecie(pet.Especie), ":)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Tu mascota ha muerto :( ", ":(", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void btDetalles_Click(object sender, EventArgs e)
        {
            panelDetalles.Visible = true;

            if (!pet.checkarMuerte())
            {
                txtDetalles.Text = "Hambre: " + pet.Hambre + "\n" +
                "Suciedad: " +  pet.Suciedad + "\n" +
                "Felicidad: " + pet.Felicidad + "\n";
            }
            else
            {
                txtDetalles.Text = "Status actual: MUERTO :(";
            }
        }
        private void btOk_Click(object sender, EventArgs e)
        {
            panelDetalles.Visible = false;
        }
    }
}
