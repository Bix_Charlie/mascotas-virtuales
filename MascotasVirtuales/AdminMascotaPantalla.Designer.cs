﻿namespace MascotasVirtuales
{
    partial class AdminMascotaPantalla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btPerro = new System.Windows.Forms.Button();
            this.btGato = new System.Windows.Forms.Button();
            this.btConejo = new System.Windows.Forms.Button();
            this.btRaton = new System.Windows.Forms.Button();
            this.btDragon = new System.Windows.Forms.Button();
            this.btAceptar = new System.Windows.Forms.Button();
            this.txtNombreAnimal = new System.Windows.Forms.TextBox();
            this.gbNombre = new System.Windows.Forms.GroupBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.gbNombre.SuspendLayout();
            this.SuspendLayout();
            // 
            // btPerro
            // 
            this.btPerro.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPerro.Location = new System.Drawing.Point(27, 75);
            this.btPerro.Name = "btPerro";
            this.btPerro.Size = new System.Drawing.Size(96, 44);
            this.btPerro.TabIndex = 0;
            this.btPerro.Text = "Perro";
            this.btPerro.UseVisualStyleBackColor = true;
            this.btPerro.Click += new System.EventHandler(this.btPerro_Click);
            // 
            // btGato
            // 
            this.btGato.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGato.Location = new System.Drawing.Point(239, 75);
            this.btGato.Name = "btGato";
            this.btGato.Size = new System.Drawing.Size(96, 44);
            this.btGato.TabIndex = 1;
            this.btGato.Text = "Gato";
            this.btGato.UseVisualStyleBackColor = true;
            this.btGato.Click += new System.EventHandler(this.btGato_Click);
            // 
            // btConejo
            // 
            this.btConejo.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btConejo.Location = new System.Drawing.Point(134, 139);
            this.btConejo.Name = "btConejo";
            this.btConejo.Size = new System.Drawing.Size(96, 44);
            this.btConejo.TabIndex = 2;
            this.btConejo.Text = "Conejo";
            this.btConejo.UseVisualStyleBackColor = true;
            this.btConejo.Click += new System.EventHandler(this.btConejo_Click);
            // 
            // btRaton
            // 
            this.btRaton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRaton.Location = new System.Drawing.Point(27, 204);
            this.btRaton.Name = "btRaton";
            this.btRaton.Size = new System.Drawing.Size(96, 44);
            this.btRaton.TabIndex = 3;
            this.btRaton.Text = "Raton";
            this.btRaton.UseVisualStyleBackColor = true;
            this.btRaton.Click += new System.EventHandler(this.btRaton_Click);
            // 
            // btDragon
            // 
            this.btDragon.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDragon.Location = new System.Drawing.Point(239, 204);
            this.btDragon.Name = "btDragon";
            this.btDragon.Size = new System.Drawing.Size(96, 44);
            this.btDragon.TabIndex = 4;
            this.btDragon.Text = "Dragon";
            this.btDragon.UseVisualStyleBackColor = true;
            this.btDragon.Click += new System.EventHandler(this.btDragon_Click);
            // 
            // btAceptar
            // 
            this.btAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAceptar.Location = new System.Drawing.Point(73, 104);
            this.btAceptar.Name = "btAceptar";
            this.btAceptar.Size = new System.Drawing.Size(116, 55);
            this.btAceptar.TabIndex = 5;
            this.btAceptar.Text = "Aceptar";
            this.btAceptar.UseVisualStyleBackColor = true;
            this.btAceptar.Click += new System.EventHandler(this.btAceptar_Click);
            // 
            // txtNombreAnimal
            // 
            this.txtNombreAnimal.Location = new System.Drawing.Point(53, 63);
            this.txtNombreAnimal.Name = "txtNombreAnimal";
            this.txtNombreAnimal.Size = new System.Drawing.Size(150, 22);
            this.txtNombreAnimal.TabIndex = 6;
            // 
            // gbNombre
            // 
            this.gbNombre.Controls.Add(this.lblNombre);
            this.gbNombre.Controls.Add(this.txtNombreAnimal);
            this.gbNombre.Controls.Add(this.btAceptar);
            this.gbNombre.Location = new System.Drawing.Point(41, 294);
            this.gbNombre.Name = "gbNombre";
            this.gbNombre.Size = new System.Drawing.Size(264, 193);
            this.gbNombre.TabIndex = 7;
            this.gbNombre.TabStop = false;
            this.gbNombre.Visible = false;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Perpetua", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(67, 28);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(130, 32);
            this.lblNombre.TabIndex = 7;
            this.lblNombre.Text = "-Nombre-";
            this.lblNombre.Click += new System.EventHandler(this.label1_Click);
            // 
            // AdminMascotaPantalla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 499);
            this.Controls.Add(this.gbNombre);
            this.Controls.Add(this.btDragon);
            this.Controls.Add(this.btRaton);
            this.Controls.Add(this.btConejo);
            this.Controls.Add(this.btGato);
            this.Controls.Add(this.btPerro);
            this.Name = "AdminMascotaPantalla";
            this.Text = "AdminMascotaPantalla";
            this.gbNombre.ResumeLayout(false);
            this.gbNombre.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btPerro;
        private System.Windows.Forms.Button btGato;
        private System.Windows.Forms.Button btConejo;
        private System.Windows.Forms.Button btRaton;
        private System.Windows.Forms.Button btDragon;
        private System.Windows.Forms.Button btAceptar;
        private System.Windows.Forms.TextBox txtNombreAnimal;
        private System.Windows.Forms.GroupBox gbNombre;
        private System.Windows.Forms.Label lblNombre;
    }
}