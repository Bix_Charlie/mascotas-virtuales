﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MascotasVirtuales
{
    public class Mascota 
    {
        private string color;
        private string nombre;
        private int especie;
        private int hambre, suciedad;
        private int felicidad;
        private bool vivo;

        //Constantes para determinar los limites de suciedad y hambre
        //Se utilizan mas adelante en la funcion Morir() para checkar si la mascota tiene EXTREMA HAMBRE o EXTREMA SUCIEDAD
        //Si hambre o suciedad sobrepasan estos valores, la mascota podria morir
        private const int hambreExtrema = 5;
        private const int suciedadExtrema = 5;
        //Se usa mas tarde para ponerle un techo a la felicidad de una mascota
        private const int felicidadMaxima = 5;
        private const float probabilidadDePopo = 33.333f;

        public Mascota()
        {
            color = "NULL";
            nombre = "NULL";
            vivo = true;
            especie = 0;
        }

        public Mascota(string color, string nombre, int especie)
        {
            this.color = color;
            this.nombre = nombre;
            this.especie = especie;
            vivo = true;
        }
        public string Color
        {
            set
            {
                color = value;
            }
            get
            {
                return color;
            }
        }

        public string Nombre
        {
            set
            {
                nombre = value;
            }
            get
            {
                return nombre;
            }
        }

        public int Especie
        {
            set
            {
                especie = value;
            }
            get
            {
                return especie;
            }
        }

        public int Hambre
        {
            set
            {
                hambre = value;
            }
            get
            {
                return hambre;
            }
        }

        public int Suciedad
        {
            set
            {
                suciedad = value;
            }
            get
            {
                return suciedad;
            }
        }

        public int Felicidad
        {
            set
            {
                felicidad = value;
            }
            get
            {
                return felicidad;
            }
        }

        public void alimentar()
        {
            hambre = (hambre > 0) ? hambre-- : hambre;
            aumentarFelicidad();
            hacerPoPo();
        }

        //Esta funcion se llama algunas veces en las otras funciones
        //Tiene una probabiliad definida como constante para hacer popo o no
        private void hacerPoPo()
        {
            Random randomizador = new Random();
            if ((randomizador.Next(100))+1 <= probabilidadDePopo)
            {
                hambre++;
                suciedad++;
                Console.WriteLine("Ooops, me he hecho popo y me he ensuciado");
            }
        }

        public void jugar()
        {
            aumentarFelicidad();
            hambre++;
            hacerPoPo();
        }

        public void bañar()
        {
            suciedad = (suciedad > 0) ? suciedad-- : suciedad;
        }

        private void aumentarFelicidad()
        {
            felicidad = (felicidad < felicidadMaxima) ? felicidad++ : felicidad;
        }

        public bool checkarMuerte()
        {
            int contador = 0;
            if (felicidad == 0) contador++;
            if (hambre >= hambreExtrema) contador++;
            if (suciedad >= suciedadExtrema) contador++;
            return contador >= 2;
        }
        public enum especies
        {
            perro = 1,
            gato,
            raton,
            conejo,
            dragon
        }

        public string checkarEspecie(int especie)
        {
            string resultado = "";
            switch (especie)
            {
                case (int)especies.perro:
                    resultado += "perro";
                    break;
                case (int)especies.gato:
                    resultado += "gato";
                    break;
                case (int)especies.raton:
                    resultado += "raton";
                    break;
                case (int)especies.conejo:
                    resultado += "conejo";
                    break;
                case (int)especies.dragon:
                    resultado += "dragon";
                    break;
                default:
                    resultado += "NULL";
                    break;
            }

            return resultado;
        }
    }
}
